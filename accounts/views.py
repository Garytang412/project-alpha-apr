from django.shortcuts import redirect, render
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login

# Create your views here.


def signup_view(request):
    # GET
    if request.method == "GET":
        form = UserCreationForm()
    # POST
    else:
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect("home")
    # return render :
    return render(request, "registration/signup.html", {"form": form})
